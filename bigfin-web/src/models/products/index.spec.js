import subject from './index';
import bigfinApi from '../../clients/bigfinApi';

jest.mock('../../clients/bigfinApi');

describe('products model', () => {
  describe('initial state', () => {
    it('sets loading as false', () => {
      expect(subject.state.loading).toEqual(false);
    });

    it('set items as empty array', () => {
      expect(subject.state.items).toEqual([]);
    });
  });

  describe('reducers', () => {
    it('returns new items with loading as false on setItem', () => {
      const newItems = ['new item'];
      expect(subject.reducers.setItems({}, newItems)).toEqual({
        items: newItems,
      });
    });

    it('adds new item on addItem', () => {
      const state = { items: [1] };
      expect(subject.reducers.addItem(state, 2)).toEqual({
        items: [1, 2],
      });
    });

    it('sets specific loading on setLoading', () => {
      const currentState = { items: 'items' };
      expect(subject.reducers.setLoading(currentState, 'loading')).toEqual({
        items: currentState.items,
        loading: 'loading',
      });
    });

    it('sets creation status as true on setCreationSuccess', () => {
      const currentState = { items: 'items' };
      expect(subject.reducers.setCreationSuccess(currentState, null)).toEqual({
        items: currentState.items,
        creation: {
          success: true,
        } 
      });
    });

    it('sets creation status as false on setCreationError', () => {
      const currentState = { items: 'items' };
      expect(subject.reducers.setCreationError(currentState, null)).toEqual({
        items: currentState.items,
        creation: {
          success: false,
        } 
      });
    });

    it('sets as already fetched on setAsAlreadyFetched', () => {
      const currentState = { items: 'items' };
      expect(subject.reducers.setAsAlreadyFetched(currentState, null)).toEqual({
        items: currentState.items,
        alreadyFetched: true,
      });
    });
  });

  describe('effects', () => {
    const state = {
      products: {
        alreadyFetched: false
      }
    };

    beforeEach(() => {
      state.user = { token: 'token' };
      subject.reducers.addItem = jest.fn();
      subject.reducers.setItems = jest.fn();
      subject.reducers.setLoading = jest.fn();
      subject.reducers.setCreationSuccess = jest.fn();
      subject.reducers.setCreationError = jest.fn();
      subject.reducers.setAsAlreadyFetched = jest.fn();
    });

    describe('fetch products', () => {
      beforeEach(() => {
        const mockApiResponse = {
          data: {
            products: 'products list',
          }
        };
        bigfinApi.fetchProducts.mockReturnValue(new Promise((resolve) => {
          return resolve(mockApiResponse);
        }));
      });

      it('performs fetch with correct token', () => {
        subject.effects.fetchProducts.call(subject.reducers, null, state);
        expect(bigfinApi.fetchProducts).toBeCalledWith('token');
      });

      it('sets products on store', async () => {
        await subject.effects.fetchProducts.call(subject.reducers, null, state);
        expect(subject.reducers.setItems).toBeCalledWith('products list');
      });

      it('sets loading as true before request completes', () => {
        subject.effects.fetchProducts.call(subject.reducers, null, state);
        expect(subject.reducers.setLoading).toBeCalledWith(true);
      });

      it('sets loading as false after request completes', async () => {
        await subject.effects.fetchProducts.call(subject.reducers, null, state);
        expect(subject.reducers.setLoading).toBeCalledWith(false);
      });

      it('sets as already fetched after request completes', async () => {
        await subject.effects.fetchProducts.call(subject.reducers, null, state);
        expect(subject.reducers.setAsAlreadyFetched).toBeCalledWith();
      });
    });

    describe('create product', () => {
      beforeEach(() => {
        const mockApiResponse = {
          data: {
            product: 'new product',
          }
        };
        bigfinApi.createProduct.mockReturnValue(new Promise((resolve) => {
          return resolve(mockApiResponse);
        }));
      });

      it('performs request with correct token', () => {
        subject.effects.createProduct.call(subject.reducers, 'new product', state);
        expect(bigfinApi.createProduct).toBeCalledWith('token', 'new product');
      });

      it('add product to store', async () => {
        await subject.effects.createProduct.call(subject.reducers, null, state);
        expect(subject.reducers.addItem).toBeCalledWith('new product');
      });

      it('sets loading as true before request completes', () => {
        subject.effects.createProduct.call(subject.reducers, null, state);
        expect(subject.reducers.setLoading).toBeCalledWith(true);
      });

      it('sets loading as false after request completes', async () => {
        await subject.effects.createProduct.call(subject.reducers, null, state);
        expect(subject.reducers.setLoading).toBeCalledWith(false);
      });

      it('sets creation as success after request completes', async () => {
        await subject.effects.createProduct.call(subject.reducers, null, state);
        expect(subject.reducers.setCreationSuccess).toBeCalledWith();
      });
    });
  });
});
