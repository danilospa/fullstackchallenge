import bigfinApi from '../../clients/bigfinApi';

export default {
  state: {
    loading: false,
    items: [],
    creation: {
      success: false,
    },
    alreadyFetched: false,
  },
  reducers: {
    setItems: (state, payload) => Object.assign({}, state, { items: payload }),
    addItem: (state, payload) => Object.assign({}, state, { items: state.items.concat([payload]) }),
    setLoading: (state, payload) => Object.assign({}, state, { loading: payload }),
    setCreationSuccess: (state, payload) => Object.assign({}, state, { creation: { success: true }}),
    setCreationError: (state, payload) => Object.assign({}, state, { creation: { success: false }}),
    setAsAlreadyFetched: (state, payload) => Object.assign({}, state, { alreadyFetched: true }),
  },
  effects: {
    async fetchProducts(payload, rootState) {
      if (rootState.products.alreadyFetched) {
        return;
      }
      const { token } = rootState.user;
      this.setLoading(true);
      const response = await bigfinApi.fetchProducts(token);
      this.setLoading(false);
      this.setItems(response.data.products);
      this.setAsAlreadyFetched();
    },
    async createProduct(payload, rootState) {
      const { token } = rootState.user;
      this.setLoading(true);
      const response = await bigfinApi.createProduct(token, payload);
      this.setLoading(false);
      this.setCreationSuccess();
      this.addItem(response.data.product);
    }
  }
};
