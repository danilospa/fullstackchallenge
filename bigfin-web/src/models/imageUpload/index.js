import bigfinApi from '../../clients/bigfinApi';

export default {
  state: {
    loading: false,
    success: false,
    url: null,
  },
  reducers: {
    setUrl: (state, payload) => Object.assign({}, state, { url: payload }),
    setSuccess: (state, payload) => Object.assign({}, state, { success: true }),
    setLoading: (state, payload) => Object.assign({}, state, { loading: payload }),
  },
  effects: {
    async uploadImage(data, rootState) {
      const { token } = rootState.user;
      this.setLoading(true);
      const response = await bigfinApi.uploadImage(token, data);
      this.setUrl(response.data.url);
      this.setLoading(false);
      this.setSuccess(true);
    },
  }
};
