import { dispatch } from '@rematch/core';
import bigfinApi from '../../clients/bigfinApi';

export default {
  state: {
    loading: false,
    success: false,
    orderData: null,
  },
  reducers: {
    setSuccess: (state, payload) => Object.assign({}, state, { success: payload }),
    setLoading: (state, payload) => Object.assign({}, state, { loading: payload }),
    setOrderData: (state, payload) => Object.assign({}, state, { orderData: payload }),
  },
  effects: {
    async createOrder(data, rootState) {
      const { token } = rootState.user;
      const { items } = rootState.cart;

      this.setLoading(true);
      const newData = Object.assign({}, data, {
        items,
      });
      try {
        await bigfinApi.createOrder(token, newData);
        this.setOrderData(Object.assign({}, newData, { cart: rootState.cart.cartCalculated }));
        this.setSuccess(true);
        dispatch.cart.clear();
      } catch (e) {
        this.setSuccess(false);
      } finally {
        this.setLoading(false);
      }
    },
  }
};
