import bigfinApi from '../../clients/bigfinApi';

export default {
  state: {
    loading: false,
    items: [],
    cartCalculated: {},
    addedItem: null,
    error: false,
  },
  reducers: {
    setItems: (state, payload) => Object.assign({}, state, { items: payload }),
    mountCart: (state, payload) => Object.assign({}, state, { cartCalculated: payload }),
    addToCart: (state, payload) => Object.assign({}, state, { items: state.items.concat(payload) }),
    setLoading: (state, payload) => Object.assign({}, state, { loading: payload }),
    setError: (state, payload) => Object.assign({}, state, { error: true }),
    setLastAdded: (state, payload) => Object.assign({}, state, { error: false, addedItem: payload }),
    clear: (state, payload) => Object.assign({}, state, { cartCalculated: {}, items: [] }),
  },
  effects: {
    async calculateCart(payload, rootState) {
      const { token } = rootState.user;
      const { items } = rootState.cart;

      this.setLoading(true);
      try {
        const existingItemIndex = rootState.cart.items.findIndex((i) => i.id === payload.id);
        let clonedItems = items.slice();
        if (existingItemIndex !== -1) {
          clonedItems[existingItemIndex].quantity += payload.quantity;
        } else {
          clonedItems.push(payload);
        }
        const response = await bigfinApi.calculateCart(token, clonedItems);
        this.addToCart(payload);
        this.setLastAdded(payload);
        this.mountCart(response.data);
      }
      catch (e) {
        this.setError();
      }
      finally {
        this.setLoading(false);
      }
    },
    async fetchCart(payload, rootState) {
      const { token } = rootState.user;
      this.setLoading(true);
      const response = await bigfinApi.getCart(token);
      this.mountCart(response.data);
      this.setLoading(false);
      const items = response.data.items.map((i) => ({
        id: i.product.id,
        quantity: i.quantity,
      }));
      this.setItems(items);
    },
  }
};
