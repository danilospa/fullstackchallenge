import subject from './index';
import bigfinApi from '../../clients/bigfinApi';

jest.mock('../../clients/bigfinApi');

describe('cart model', () => {
  describe('initial state', () => {
    it('sets loading as false', () => {
      expect(subject.state.loading).toEqual(false);
    });

    it('set items as empty array', () => {
      expect(subject.state.items).toEqual([]);
    });

    it('sets cart calculated as empty object', () => {
      expect(subject.state.cartCalculated).toEqual({});
    });
  });

  describe('reducers', () => {
    const currentState = { loading: false, items: [], cartCalculated: 'cart' };

    it('returns specified calculated cart on mountCart', () => {
      const calculatedCart = 'calculated cart';
      expect(subject.reducers.mountCart(currentState, calculatedCart)).toEqual({
        loading: false,
        items: [],
        cartCalculated: 'calculated cart',
      });
    });

    it('adds new item on addToCart', () => {
      expect(subject.reducers.addToCart(currentState, 2)).toEqual({
        loading: false,
        items: [2],
        cartCalculated: 'cart',
      });
    });

    it('sets specific loading on setLoading', () => {
      expect(subject.reducers.setLoading(currentState, 'loading')).toEqual({
        items: currentState.items,
        loading: 'loading',
        cartCalculated: currentState.cartCalculated,
      });
    });
  });

  describe('effects', () => {
    const state = {
      cart: {
        items: [{ id: 1 }]
      }
    };

    beforeEach(() => {
      state.user = { token: 'token' };
      subject.reducers.setItems = jest.fn();
      subject.reducers.mountCart = jest.fn();
      subject.reducers.addToCart = jest.fn();
      subject.reducers.setLoading = jest.fn();
      subject.reducers.setError = jest.fn();
      subject.reducers.setLastAdded = jest.fn();
    });

    describe('calculate cart', () => {
      beforeEach(() => {
        const mockApiResponse = {
          data: {
            cart: 'cart',
          }
        };
        bigfinApi.calculateCart.mockReturnValue(new Promise((resolve) => {
          return resolve(mockApiResponse);
        }));
      });

      it('performs request with correct token', () => {
        subject.effects.calculateCart.call(subject.reducers, 'new item', state);
        expect(bigfinApi.calculateCart).toBeCalledWith('token', state.cart.items.concat(['new item']));
      });

      it('sets loading as true before request completes', () => {
        subject.effects.calculateCart.call(subject.reducers, null, state);
        expect(subject.reducers.setLoading).toBeCalledWith(true);
      });

      it('sets loading as false after request completes', async () => {
        await subject.effects.calculateCart.call(subject.reducers, null, state);
        expect(subject.reducers.setLoading).toBeCalledWith(false);
      });

      it('mounts cart after request completes', async () => {
        await subject.effects.calculateCart.call(subject.reducers, 'new item', state);
        expect(subject.reducers.mountCart).toBeCalledWith({ cart: 'cart' });
      });
    });
  });
});
