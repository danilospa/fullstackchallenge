import subject from './index';
import { dispatch } from '@rematch/core';
import bigfinApi from '../../clients/bigfinApi';

jest.mock('../../clients/bigfinApi');
jest.mock('@rematch/core');

describe('user model', () => {
  describe('initial state', () => {
    it('sets loading as false', () => {
      expect(subject.state.loading).toEqual(false);
    });

    it('sets name as null', () => {
      expect(subject.state.name).toEqual(null);
    });

    it('sets token as authToken from localstorage', () => {
      expect(subject.state.token).toEqual(localStorage.__STORE__['authToken']);
    });
  });

  describe('reducers', () => {
    it('returns specified user', () => {
      const newUser = {
        name: 'maria',
        age: 10,
      };
      expect(subject.reducers.setUser(null, newUser)).toEqual(newUser);
    });

    it('sets specific loading on setLoading', () => {
      const currentUser = { name: 'john' };
      expect(subject.reducers.setLoading(currentUser, 'loading')).toEqual({
        name: 'john',
        loading: 'loading',
      });
    });
  });

  describe('effects', () => {
    beforeEach(() => {
      subject.reducers.setUser = jest.fn();
      subject.reducers.setLoading = jest.fn();
      Object.assign(dispatch, { cart: { fetchCart: jest.fn() }})
    });

    describe('facebook login', () => {
      beforeEach(() => {
        const mockApiResponse = {
          data: {
            user: { name: 'john' }
          }
        };
        bigfinApi.facebookLogin.mockReturnValue(new Promise((resolve) => {
          return resolve(mockApiResponse);
        }));
      });

      it('performs login with correct token', () => {
        subject.effects.facebookLogin.call(subject.reducers, 'token', null);
        expect(bigfinApi.facebookLogin).toBeCalledWith('token');
      });

      it('sets user on store', async () => {
        await subject.effects.facebookLogin.call(subject.reducers, 'token', null);
        expect(subject.reducers.setUser).toBeCalledWith({ name: 'john', token: 'token' });
      });

      it('sets loading as true before request completes', () => {
        subject.effects.facebookLogin.call(subject.reducers, 'token', null);
        expect(subject.reducers.setLoading).toBeCalledWith(true);
      });

      it('sets loading as false after request completes', async () => {
        await subject.effects.facebookLogin.call(subject.reducers, 'token', null);
        expect(subject.reducers.setLoading).toBeCalledWith(false);
      });
    });

    describe('fetch session', () => {
      const token = 'token';

      beforeEach(() => {
        const mockApiResponse = {
          data: {
            name: 'john'
          }
        };
        bigfinApi.fetchCurrentSession.mockReturnValue(new Promise((resolve) => {
          return resolve(mockApiResponse);
        }));
      });

      it('performs fetch with correct token', () => {
        subject.effects.fetchSession.call(subject.reducers, null, { user: { token }});
        expect(bigfinApi.fetchCurrentSession).toBeCalledWith('token');
      });

      it('sets user on store', async () => {
        await subject.effects.fetchSession.call(subject.reducers, null, { user: { token }});
        expect(subject.reducers.setUser).toBeCalledWith({ name: 'john', token: 'token' });
      });

      it('sets loading as true before request completes', () => {
        subject.effects.fetchSession.call(subject.reducers, null, { user: { token }});
        expect(subject.reducers.setLoading).toBeCalledWith(true);
      });

      it('sets loading as false after request completes', async () => {
        await subject.effects.fetchSession.call(subject.reducers, null, { user: { token }});
        expect(subject.reducers.setLoading).toBeCalledWith(false);
      });
    });
  });
});
