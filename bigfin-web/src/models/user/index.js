import { dispatch } from '@rematch/core';
import bigfinApi from '../../clients/bigfinApi';

export default {
  state: {
    loading: false,
    name: null,
    token: localStorage.getItem('authToken'),
  },
  reducers: {
    setUser: (state, payload) => payload,
    setLoading: (state, payload) => Object.assign({}, state, { loading: payload }),
  },
  effects: {
    async facebookLogin(token, rootState) {
      this.setLoading(true);
      const response = await bigfinApi.facebookLogin(token);
      this.setLoading(false);
      this.setUser(Object.assign({}, response.data.user, { token }));
      dispatch.cart.fetchCart();
    },
    async fetchSession(payload, rootState) {
      const { token } = rootState.user;
      this.setLoading(true);
      const response = await bigfinApi.fetchCurrentSession(token);
      this.setLoading(false);
      this.setUser(Object.assign({}, response.data, { token }));
      dispatch.cart.fetchCart();
    }
  }
};
