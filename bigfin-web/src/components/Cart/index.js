import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { Container, Row, Col, Card, CardBody, CardText } from 'reactstrap';
import CartItem from '../CartItem/index';
import Price from '../Price/index';
import CheckoutForm from '../CheckoutForm/index';
import './index.css';

class Cart extends React.Component {
  render() {
    const { cart } = this.props;

    if (!cart.items || !cart.items.length) {
      return (
        <Container>
          Ops, você não possui items no carrinho. Volte para o <Link to="/">catálogo</Link> e adicione algum.
        </Container>
      );
    }

    return (
      <Container>
        <Row>
          <Col md="8">
            { cart.items && cart.items.map((i) => (
              <CartItem key={i.product.id} item={i}/>
            ))}
          </Col>
          <Col md="4">
            <Row>
              <Col md="12">
                <Card className="cart__price-card">
                  <CardBody>
                    <CardText>
                      Subtotal: <Price value={ cart.subTotal } />
                    </CardText>

                    <CardText>
                      Descontos: <Price value={ cart.discountsByFactor } />
                    </CardText>

                    <CardText>
                      Total: <Price value={ cart.total } />
                    </CardText>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col md="12">
                <CheckoutForm />
              </Col>
            </Row>
          </Col>
        </Row>

      </Container>
    );
  }
};

const mapState = ({ cart }) => ({
  cart: cart.cartCalculated,
});

export default connect(mapState)(Cart);
