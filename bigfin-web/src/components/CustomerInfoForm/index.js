import React from 'react';
import FormattedInput from "@buttercup/react-formatted-input";
import { CardTitle, FormGroup, Label } from 'reactstrap';
import * as formats from '../../helpers/input-formats';
import './index.css';

class CustomerInfoForm extends React.Component {
  handleUpdateOnFormattedInput(field, formattedValue, rawValue) {
    const data = {};
    data[field] = rawValue;
    this.props.updateCustomerInfo(Object.assign({}, this.props, data));
  }

  render() {
    return (
      <div className="customer-info-form">
        <CardTitle>Dados de usuário</CardTitle>

        <FormGroup>
          <Label>CPF</Label>
          <FormattedInput placeholder="000.000.000-00" format={formats.CPF} value={this.props.documentNumber} onChange={(...args) => this.handleUpdateOnFormattedInput('documentNumber', ...args)} className="form-control" />
        </FormGroup>

        <FormGroup>
          <Label>Telefone</Label>
          <FormattedInput placeholder="(00) 00000-0000" format={formats.PHONE} value={this.props.phoneNumber} onChange={(...args) => this.handleUpdateOnFormattedInput('phoneNumber', ...args)} className="form-control" />
        </FormGroup>

      </div>
    );
  }
};

export default CustomerInfoForm;
