import React from 'react';
import Loader from '../Loader/index';
import './index.css';

class FacebookLogin extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.facebookLogin = this.facebookLogin.bind(this);
    this.state = {
      loading: false,
    };
  }

  componentDidMount() {
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12&appId=1937888326452023&autoLogAppEvents=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  facebookLogin(response) {
    const { authResponse } = response;
    this.setState({ loading: false });
    if (authResponse) {
      this.props.facebookLogin(authResponse.accessToken);
    }
  }

  handleClick() {
    this.setState({ loading: true });
    window.FB.login(this.facebookLogin);
  }


  render() {
    const { loading } = this.state;
    const modifierClass = loading && '--disabled';
    return (
      <button className={`facebook-login__button ${modifierClass}`} onClick={this.handleClick} disabled={loading}>
        <Loader show={loading} />
        Fazer login com Facebook
      </button>
    );
  }
};

export default FacebookLogin;
