import React from 'react';
import Product from '../Product/index';
import { Container, Row } from 'reactstrap';

class ProductsList extends React.Component {
  render() {
    const products = this.props.products.items.map((p) => (
      <Product key={p.id} product={p} />
    ));

    return (
      <Container>
        <Row>
          {products}
        </Row>
      </Container>
    );
  }
};

export default ProductsList;
