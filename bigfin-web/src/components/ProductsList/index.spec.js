import React from 'react';
import { shallow } from 'enzyme';
import ProductsList from './index';

describe('ProductsList', () => {
  let component, products;

  beforeEach(() => {
    products = {
      items: [{ id: 1 }]
    };
    component = shallow(<ProductsList products={products} />);
  });

  it('passes correct props to Product', () => {
    expect(component.find('Connect(Connect(Product))').props().product).toEqual(products.items[0]);
  });
});
