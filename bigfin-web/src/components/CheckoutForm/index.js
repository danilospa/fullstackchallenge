import React from 'react';
import { connect } from 'react-redux';
import { Form, Button, Card, CardBody } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import CustomerInfoForm from '../CustomerInfoForm/index';
import CreditCardForm from '../CreditCardForm/index';
import AddressForm from '../AddressForm/index';
import Loader from '../Loader';
import './index.css';

class CheckoutForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleCustomerInfoUpdate = this.handleCustomerInfoUpdate.bind(this);
    this.handleCreditCardUpdate = this.handleCreditCardUpdate.bind(this);
    this.handleAddressUpdate = this.handleAddressUpdate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      customerInfo: {
        documentNumber: '',
        phoneNumber: '',
      },
      creditCardInfo: {
        cardHolderName: '',
        cardNumber: '',
        cardExpirationDate: '',
        cardCvv: '',
      },
      shippingAddress: {
        street: '',
        streetNumber: '',
        zipcode: '',
        neighborhood: '',
        city: '',
        state: '',
      },
      submitted: false,
    };
  }

  handleCustomerInfoUpdate(customerInfo) {
    const newCustomerInfo = Object.assign({}, this.state.customerInfo, customerInfo);
    this.setState(Object.assign({}, this.state, { customerInfo: newCustomerInfo }));
  }

  handleCreditCardUpdate(creditCardInfo) {
    const newCreditCardInfo = Object.assign({}, this.state.creditCardInfo, creditCardInfo);
    this.setState(Object.assign({}, this.state, { creditCardInfo: newCreditCardInfo }));
  }

  handleAddressUpdate(addressInfo) {
    const newAddressInfo = Object.assign({}, this.state.shippingAddress, addressInfo);
    this.setState(Object.assign({}, this.state, { shippingAddress: newAddressInfo }));
  }

  handleSubmit(event) {
    event.preventDefault();
    const { phoneNumber } = this.state.customerInfo;
    const customerInfo = Object.assign({}, this.state.customerInfo, { phoneNumber: `+55${phoneNumber}`});
    this.props.createOrder(Object.assign({}, this.state, { customerInfo }));
    this.setState({ submitted: true });
  }

  render() {
    if (this.state.submitted && this.props.success) {
      return (<Redirect push to="/checkout/success"/>)
    }

    return (
      <Card>
        <CardBody>
          <Form onSubmit={this.handleSubmit}>
            <CustomerInfoForm updateCustomerInfo={this.handleCustomerInfoUpdate}/>
            <br/>
            <CreditCardForm updateCreditCardInfo={this.handleCreditCardUpdate}/>
            <br/>
            <AddressForm updateAddressInfo={this.handleAddressUpdate}/>
            { this.state.submitted && !this.props.success && !this.props.loading &&
              <p className="checkout-form__error-message">Ocorreu algum erro ao finalizar a compra, verifique seus dados.</p>
            }
            <Button color={this.props.loading ? 'secondary' : 'primary'} block={true} disabled={this.props.loading}>
              <Loader show={this.props.loading}/>
              Fechar compra
            </Button>
          </Form>
        </CardBody>
      </Card>
    );
  }
};

const mapState = ({ order }) => ({
  loading: order.loading,
  success: order.success,
});

const mapDispatch = ({ order: { createOrder } }) => ({
  createOrder,
});

export default connect(mapState, mapDispatch)(CheckoutForm);
