import React from 'react';
import { connect } from 'react-redux';
import { CardText, CardTitle, Container, Row, Col, Card, CardBody } from 'reactstrap';
import { Link } from 'react-router-dom';
import Price from '../Price/index';
import './index.css';

class CheckoutSuccess extends React.Component {
  render() {
    const { orderData } = this.props;
    const { cart } = orderData;

    console.log(this.props);
    return (
      <Container className="checkout-success">
        <Card>
          <CardBody>
            <CardTitle>Compra finalizada com sucesso!</CardTitle>
            <CardText>Valor: <Price value={cart.total}/></CardText>
            <hr/>

            <Row>
              <Col md="6">
                <CardText>Endereço de entrega:</CardText>
                <CardText className="checkout-success__text">{orderData.shippingAddress.street}, {orderData.shippingAddress.streetNumber}</CardText>
                <CardText className="checkout-success__text">{orderData.shippingAddress.neighborhood}</CardText>
                <CardText className="checkout-success__text">{orderData.shippingAddress.city}/{orderData.shippingAddress.state}</CardText>
                <CardText className="checkout-success__text">{orderData.shippingAddress.zipcode}</CardText>
              </Col>
              <Col md="6">
                <CardText>Forma de pagamento</CardText>
                <CardText className="checkout-success__text">Cartão final {orderData.creditCardInfo.cardNumber.substr(12)}</CardText>
                <CardText className="checkout-success__text">{orderData.creditCardInfo.cardHolderName}</CardText>
              </Col>
            </Row>

            <hr/>
            <Link to="/" className="btn btn-primary">Voltar para o catálogo</Link>
          </CardBody>
        </Card>
      </Container>
    );
  }
};

const mapState = ({ order: { orderData }}) => ({
  orderData,
});

export default connect(mapState)(CheckoutSuccess);
