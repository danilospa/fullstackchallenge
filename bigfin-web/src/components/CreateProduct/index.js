import React from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col, Card, CardBody } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import Loader from '../Loader';
import './index.css';

class CreateProduct extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.props.resetImageUrl();
    this.state = {
      name: '',
      description: '',
      price: '',
      image: '',
      factor: 'A',
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return { image: nextProps.imageUrl };
  }

  updateInputValue(event) {
    const newState = {}
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.createProduct(this.state);
  }

  componentDidUpdate() {
    if (this.props.success) {
      this.props.resetCreationStatus();
    }
  }

  uploadImage(event) {
    const { files } = event.target;
    const data = new FormData();
    data.append('file', files[0]);
    this.props.uploadImage(data);
  }

  render() {
    if (this.props.success) {
      return (<Redirect push to="/" />)
    }

    return (
      <Container>
        <Row>
          <Col md={{ size: 6, offset: 1 }}>
            <Card>
              <CardBody>
                <Form onSubmit={this.handleSubmit}>
                  <h2>Criar produto</h2>

                  <FormGroup>
                    <Label for="name">Nome</Label>
                    <Input type="text" id="name" placeholder="Nome do produto" value={this.state.name} onChange={evt => this.updateInputValue(evt)} required />
                  </FormGroup>

                  <FormGroup>
                    <Label for="description">Descrição</Label>
                    <Input type="textarea" id="description" placeholder="Descrição do produto" value={this.state.description} onChange={evt => this.updateInputValue(evt)} required />
                  </FormGroup>

                  <FormGroup>
                    <Label for="price">Preço</Label>
                    <Input type="number" id="price" placeholder="100,00" value={this.state.price} onChange={evt => this.updateInputValue(evt)} required />
                  </FormGroup>

                  <FormGroup>
                    <Label for="imageFile">Imagem</Label>
                    <Input type="file" accept="image/*" id="imageFile" value={this.state.imageFile} onChange={evt => this.uploadImage(evt)} required />
                  </FormGroup>

                  <FormGroup>
                    <Label for="factor">Fator de desconto</Label>
                    <Input type="select" id="factor" value={this.state.factor} onChange={evt => this.updateInputValue(evt)} >
                      <option>A</option>
                      <option>B</option>
                      <option>C</option>
                    </Input>
                  </FormGroup>

                  <Button color={this.props.imageUploading? 'secondary' : 'primary'} disabled={!this.state.image}>
                    <Loader show={this.props.imageUploading} />
                    Criar produto
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
          { this.state.image &&
            <Col md="5">
              <Card>
                <CardBody>
                  <img src={this.state.image} className="create-product__image" alt=""/>
                </CardBody>
              </Card>
            </Col>
          }
        </Row>
      </Container>
    );
  }
};

const mapState = (state) => ({
  success: state.products.creation.success,
  imageUrl: state.imageUpload.url,
  imageUploading: state.imageUpload.loading,
});

const mapDispatch = ({ products: { createProduct, setCreationError }, imageUpload }) => ({
  createProduct,
  resetCreationStatus: setCreationError,
  uploadImage: imageUpload.uploadImage,
  resetImageUrl: () => imageUpload.setUrl(null),
});

const CreateProductContainer = connect(mapState, mapDispatch)(CreateProduct);

export default CreateProductContainer;
