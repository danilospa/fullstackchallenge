import React from 'react';
import { Navbar, Nav, NavItem, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import FacebookLogin from '../FacebookLogin/index';
import Loader from '../Loader/index';
import './index.css';

class AppHeader extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showAddedToCart: false,
      addedItem: null,
    };
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    return nextProps.addedItem === prevState.addedItem ? null : { showAddedToCart: true };
  }

  render() {
    const { user, facebookLogin } = this.props;

    if (this.state.showAddedToCart) {
      setTimeout(() => {
        this.setState({ showAddedToCart: false });
      }, 2500);
    };

    return (
      <div className="app-header__container">
        <Navbar color="light" light expand="md" className="app-header">
          <Link to="/" className="navbar-brand">Bigfin e-commerce</Link>

          <Nav className="ml-auto" navbar>
            { !user.token && !user.loading &&
              <FacebookLogin facebookLogin={facebookLogin}/>
            }
            { user.role === 'admin' &&
              <NavItem>
                <Link to="/products/create" className="app-header__create-product nav-link">Criar Produto</Link>
              </NavItem>
            }
            { user.name &&
              <NavItem>
                <Link to="/cart" className="app-header__cart nav-link">Meu carrinho</Link>
              </NavItem>
            }
          </Nav>
        </Navbar>
        { this.state.showAddedToCart &&
          <Alert color="success" className="app-header__alert">
            Item adicionado com sucesso!
          </Alert>
        }
        { this.props.cartError &&
          <Alert color="danger" className="app-header__alert">
            Falha ao adicionar item!
          </Alert>
        }
        { this.props.cartLoading &&
          <Alert color="info" className="app-header__alert">
            <Loader show={true}></Loader>
            Adicionando item...
          </Alert>
        }
      </div>
    );
  }
};

export default AppHeader;
