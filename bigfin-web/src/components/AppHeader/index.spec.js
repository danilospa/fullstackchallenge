import React from 'react';
import { shallow } from 'enzyme';
import AppHeader from './index';

describe('AppHeader', () => {
  let component;

  beforeEach(() => {
    component = shallow(<AppHeader user={{}} facebookLogin={'facebook login'}/>);
  });

  it('hides facebook login when user info is loading', () => {
    component.setProps({ user: { loading: true }});
    expect(component.find('FacebookLogin').length).toBe(0);
  });

  it('hides facebook login when user is logged', () => {
    component.setProps({ user: { token: 'token', loading: false }});
    expect(component.find('FacebookLogin').length).toBe(0);
  });

  it('shows facebook login when user is not logged', () => {
    component.setProps({ user: { token: null, loading: false }});
    expect(component.find('FacebookLogin').length).toBe(1);
  });

  it('hides cart link when user is not logged', () => {
    component.setProps({ user: { name: null }});
    expect(component.find('.app-header__cart').length).toBe(0);
  });

  it('shows cart link when user is logged', () => {
    component.setProps({ user: { name: 'john' }});
    expect(component.find('.app-header__cart').length).toBe(1);
  });

  it('hides create product link when user is not admin', () => {
    component.setProps({ user: { role: 'consumer' }});
    expect(component.find('.app-header__create-product').length).toBe(0);
  });

  it('shows cart link when user is logged', () => {
    component.setProps({ user: { role: 'admin' }});
    expect(component.find('.app-header__create-product').length).toBe(1);
  });

  it('pass correct prop to FacebookLogin', () => {
    expect(component.find('FacebookLogin').props().facebookLogin).toEqual('facebook login');
  });
});
