import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import Product from './index';

const mockStore = configureStore([]);

describe('Product', () => {
  let component, product, dispatch;

  beforeEach(() => {
    product = {
      id: 'product id',
      name: 'product name',
      description: 'product description',
      price: 2000,
      image: 'url for image',
      factor: 'factor'
    };
    dispatch = {
      cart: {
        addToCart: jest.fn(),
        calculateCart: jest.fn(),
      }
    };
    const user = { token: 'token' };
    const store = mockStore({ user });
    Object.assign(store.dispatch, dispatch);
    component = shallow(<Product store={store} product={product} />).dive().dive();
  });

  it('renders correct name', () => {
    expect(component.html()).toMatch(product.name);
  });

  it('renders correct description', () => {
    expect(component.html()).toMatch(product.description);
  });

  it('renders correct image', () => {
    expect(component.html()).toMatch(product.image);
  });

  it('renders correct factor', () => {
    expect(component.html()).toMatch(product.factor);
  });

  it('sets correct props on add to cart button when user is logged', () => {
    expect(component.find('Button').props()).toMatchObject({
      color: 'primary',
    });
  });

  it('shows correct text on add to cart button when user is logged in', () => {
    expect(component.find('Button').html()).toMatch('Adicionar ao carrinho');
  });

  it('calls addToCart function when clicking on button', () => {
    component.instance().addToCart = jest.fn();
    component.find('Button').simulate('click');
    expect(component.instance().addToCart).toBeCalledWith('product id', 1);
  });

  describe('when adding an item to cart', () => {
    beforeEach(() => {
      const id = 2, quantity = 5;
      component.instance().addToCart(id, quantity);
    });

    it('calculates new cart calling function from prop', () => {
      expect(dispatch.cart.calculateCart).toBeCalledWith({ id: 2, quantity: 5 });
    });
  });

  describe('when user is not logged', () => {
    beforeEach(() => {
      component.setProps({ user: { token: null }});
    });

    it('sets correct props on add to cart button', () => {
      expect(component.find('Button').props()).toMatchObject({
        size: 'sm',
        disabled: true,
      });
    });

    it('shows correct text on add to cart button', () => {
      expect(component.find('Button').html()).toMatch('Faça login para continuar');
    });
  });
});
