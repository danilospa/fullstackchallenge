import React from 'react';
import { connect } from 'react-redux';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Col } from 'reactstrap';
import Price from '../Price/index';
import UserContainer from '../../containers/User/index';
import './index.css';

class Product extends React.Component {
  addToCart(id, quantity) {
    this.props.calculateCart({ id, quantity });
  }

  render() {
    const { user, product } = this.props;
    const { id, name, description, price, image, factor } = product;

    const buttonProps = {};
    if (user.token) {
      buttonProps.color = 'primary';
    } else {
      buttonProps.size = 'sm';
      buttonProps.disabled = true;
    }

    return (
      <Col md="3" className="product">
        <Card>
          <div className="product__image-container">
            <CardImg top src={image} />
          </div>
          <CardBody>
            <CardTitle>{name}</CardTitle>
            <CardSubtitle>
              <Price value={price} />
            </CardSubtitle>

            <CardText>{description}</CardText>
            <CardText>Fator de desconto: {factor}</CardText>
            <Button {...buttonProps} block={true} onClick={() => this.addToCart(id, 1)}>{ user.token ? 'Adicionar ao carrinho' : 'Faça login para continuar' }</Button>
          </CardBody>
        </Card>
      </Col>
    );
  }
};

const mapState = (state) => ({});

const mapDispatch = ({ cart: { calculateCart } }) => ({
  calculateCart,
});

export default connect(mapState, mapDispatch)(UserContainer(Product));
