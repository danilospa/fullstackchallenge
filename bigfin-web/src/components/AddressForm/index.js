import React from 'react';
import FormattedInput from "@buttercup/react-formatted-input";
import { Row, Col, CardTitle, FormGroup, Label, Input } from 'reactstrap';
import * as formats from '../../helpers/input-formats';
import './index.css';

class AddressForm extends React.Component {
  handleInputValue(field, formattedValue, rawValue) {
    const data = {};
    data[field] = rawValue;
    this.props.updateAddressInfo(Object.assign({}, this.props, data));
  }

  render() {
    return (
      <div className="address-form">
        <CardTitle>Endereço</CardTitle>

        <FormGroup>
          <Label>CEP</Label>
          <FormattedInput placeholder="00000-000" format={formats.ZIPCODE} value={this.props.zipcode} onChange={(...args) => this.handleInputValue('zipcode', ...args)} className="form-control" />
        </FormGroup>

        <Row>
          <Col md="8">
            <FormGroup>
              <Label>Cidade</Label>
              <Input type="text" placeholder="Cidade" value={this.props.city} onChange={evt => this.handleInputValue('city', null, evt.target.value)} />
            </FormGroup>
          </Col>

          <Col md="4">
            <FormGroup>
              <Label>Estado</Label>
              <Input type="text" maxLength="2" placeholder="UF" value={this.props.state} onChange={evt => this.handleInputValue('state', null, evt.target.value)} />
            </FormGroup>
          </Col>
        </Row>

        <FormGroup>
          <Label>Bairro</Label>
          <Input type="text" id="neighborhood" placeholder="Bairro" value={this.props.neighborhood} onChange={evt => this.handleInputValue('neighborhood', null, evt.target.value)} />
        </FormGroup>

        <Row>
          <Col md="8">
            <FormGroup>
              <Label>Rua</Label>
              <Input type="text" id="street" placeholder="Av 9 de Julho" value={this.props.street} onChange={evt => this.handleInputValue('street', null, evt.target.value)} />
            </FormGroup>
          </Col>

          <Col md="4">
            <FormGroup>
              <Label>Número</Label>
              <Input type="number" id="streetNumber" placeholder="900" value={this.props.streetNumber} onChange={evt => this.handleInputValue('streetNumber', null, evt.target.value)} />
            </FormGroup>
          </Col>
        </Row>

      </div>
    );
  }
};

export default AddressForm;
