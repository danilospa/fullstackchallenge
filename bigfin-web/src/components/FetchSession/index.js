import React from 'react';

class FetchSession extends React.Component {
  componentDidMount() {
    if (this.props.user.token) {
      this.props.fetchSession();
    }
  }

  render() {
    return (null);
  }
};

export default FetchSession;
