import React from 'react';
import { shallow } from 'enzyme';
import FetchSession from './index';

describe('FetchSession', () => {
  let component, fetchSession, fetchProducts;

  beforeEach(() => {
    fetchSession = jest.fn();
    fetchProducts = jest.fn();
    component = shallow(
      <FetchSession user={{}} fetchSession={fetchSession} />
    );
  });

  it('does not fetch current session if token does not exist when mounting', () => {
    expect(fetchSession).not.toBeCalled();
  });

  it('fetches current session if token existx when mounting', () => {
    component.setProps({ user: { token: 'token' }});
    component.instance().componentDidMount();
    expect(fetchSession).toBeCalled();
  });
});
