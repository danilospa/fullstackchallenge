import React from 'react';
import FormattedInput from "@buttercup/react-formatted-input";
import { Row, Col, CardTitle, FormGroup, Label, Input } from 'reactstrap';
import * as formats from '../../helpers/input-formats';
import './index.css';

class CreditCardForm extends React.Component {
  handleInputValue(field, formattedValue, rawValue) {
    const data = {};
    data[field] = rawValue;
    this.props.updateCreditCardInfo(Object.assign({}, this.props, data));
  }

  render() {
    return (
      <div className="credit-card-form">
        <CardTitle>Cartão de Crédito</CardTitle>

        <FormGroup>
          <Label>Nome</Label>
          <Input type="text" placeholder="Nome no cartão" value={this.props.cardHolderName} onChange={evt => this.handleInputValue('cardHolderName', null, evt.target.value)} />
        </FormGroup>

        <FormGroup>
          <Label>Número</Label>
          <FormattedInput placeholder="0000 0000 0000 0000" format={formats.CREDIT_CARD_NUMBER} value={this.props.cardNumber} onChange={(...args) => this.handleInputValue('cardNumber', ...args)} className="form-control" />
        </FormGroup>

        <Row>
          <Col md="6">
            <FormGroup>
              <Label>Data de expiração</Label>
              <FormattedInput placeholder="01/20" format={formats.CREDIT_CART_EXPIRATION} value={this.props.cardExpirationDate} onChange={(...args) => this.handleInputValue('cardExpirationDate', ...args)} className="form-control" />
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <Label>CVV</Label>
              <FormattedInput placeholder="CVV" format={formats.CREDIT_CARD_CVV} value={this.props.cardCvv} onChange={(...args) => this.handleInputValue('cardCvv', ...args)} className="form-control" />
            </FormGroup>
          </Col>
        </Row>
      </div>
    );
  }
};

export default CreditCardForm;
