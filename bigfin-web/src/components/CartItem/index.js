import React from 'react';
import {
  Row, Col, Card, CardImg, CardBody, CardTitle, CardText
} from 'reactstrap';
import Price from '../Price/index';
import './index.css';

class CartItem extends React.Component {
  render() {
    const { item } = this.props;
    return (
      <Row className="cart-item">
        <Col>
          <Card>
            <div className="cart-item__image-container">
              <CardImg src={item.product.image} />
            </div>
            <CardBody>
              <CardTitle>{item.product.name}</CardTitle>

              <div className="cart-item__group">
                <CardText>
                  Preço unitário:
                  <Price value={item.product.price} />
                </CardText>
                <CardText>Fator de desconto: {item.product.factor}</CardText>
              </div>

              <div className="cart-item__group">
                <CardText>Quantidade: {item.quantity}</CardText>
                <CardText>
                  Valor:
                  <Price value={item.product.price * item.quantity} />
                </CardText>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
};

export default CartItem;
