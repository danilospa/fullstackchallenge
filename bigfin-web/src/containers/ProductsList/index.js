import { connect } from 'react-redux';
import ProductsList from '../../components/ProductsList';

const mapState = state => ({
  products: state.products,
});

const mapDispatch = ({ products }) => ({
  fetchProducts: products.fetchProducts(),
});

const ProductsListContainer = connect(mapState, mapDispatch)(ProductsList);

export default ProductsListContainer;
