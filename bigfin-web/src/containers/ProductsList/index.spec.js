import React from 'react';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import ProductsListContainer from './index';

const mockStore = configureStore([]);

describe('ProductsListContainer', () => {
  let component;

  beforeEach(() => {
    const products = 'products';
    const dispatch = {
      products: {
        fetchProducts: jest.fn().mockReturnValue('list of products'),
      }
    };
    const store = mockStore({ products });
    Object.assign(store.dispatch, dispatch);

    component = shallow(<ProductsListContainer store={store} />);
  });

  it('should pass correct products to component', () => {
    const products = component.find('ProductsList').props().products;
    expect(products).toEqual('products');
  });

  it('should pass correct dispatch to component', () => {
    const dispatch = component.find('ProductsList').props().fetchProducts;
    expect(dispatch).toEqual('list of products');
  });
});
