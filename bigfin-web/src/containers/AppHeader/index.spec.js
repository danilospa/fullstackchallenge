import React from 'react';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import AppHeaderContainer from './index';

const mockStore = configureStore([]);

describe('AppHeaderContainer', () => {
  let component;
  let dispatch;

  beforeEach(() => {
    const user = { name: 'teste' };
    const cart = {
      addedItem: null,
      loading: false,
      error: false,
    };
    dispatch = {
      user: {
        facebookLogin: 'facebook login dispatcher',
      },
    };
    const store = mockStore({ user, cart });
    Object.assign(store.dispatch, dispatch);

    component = shallow(<AppHeaderContainer store={store} />);
  });

  it('should pass correct user to component', () => {
    const user = component.find('AppHeader').props().user;
    expect(user.name).toEqual('teste');
  });

  it('should pass correct facebook login dispatch to component', () => {
    const dispatch = component.find('AppHeader').props().facebookLogin;
    expect(dispatch).toEqual('facebook login dispatcher');
  });
});
