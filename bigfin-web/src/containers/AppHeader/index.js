import { connect } from 'react-redux';
import AppHeader from '../../components/AppHeader';

const mapState = state => ({
  user: state.user,
  addedItem: state.cart.addedItem,
  cartLoading: state.cart.loading,
  cartError: state.cart.error,
});

const mapDispatch = ({ user }) => ({
  facebookLogin: user.facebookLogin,
});

const AppHeaderContainer = connect(mapState, mapDispatch)(AppHeader);

export default AppHeaderContainer;
