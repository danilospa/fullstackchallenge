import { connect } from 'react-redux';
import FetchSession from '../../components/FetchSession';

const mapState = state => ({
  user: state.user,
});

const mapDispatch = ({ user }) => ({
  fetchSession: user.fetchSession,
});

const SessionContainer = connect(mapState, mapDispatch)(FetchSession);

export default SessionContainer;
