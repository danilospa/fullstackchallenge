import React from 'react';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import SessionContainer from './index';

const mockStore = configureStore([]);

describe('SessionContainer', () => {
  let component;
  let dispatch;

  beforeEach(() => {
    const user = { name: 'teste' };
    dispatch = {
      user: {
        fetchSession: 'fetch session dispatcher',
      },
    };
    const store = mockStore({ user });
    Object.assign(store.dispatch, dispatch);

    component = shallow(<SessionContainer store={store} />);
  });

  it('should pass correct user to component', () => {
    const user = component.find('FetchSession').props().user;
    expect(user.name).toEqual('teste');
  });

  it('should pass correct fecth session dispatch to component', () => {
    const dispatch = component.find('FetchSession').props().fetchSession;
    expect(dispatch).toEqual('fetch session dispatcher');
  });
});
