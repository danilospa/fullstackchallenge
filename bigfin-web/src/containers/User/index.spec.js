import React from 'react';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import UserContainer from './index';

const mockStore = configureStore([]);

describe('UserContainer', () => {
  let component;

  beforeEach(() => {
    const user = { name: 'teste' };
    const store = mockStore({ user });

    const wrappedComponent = () => (<p>child component</p>);
    const MockComponent = UserContainer(wrappedComponent);
    component = shallow(<MockComponent store={store} />);
  });

  it('should pass correct user to component', () => {
    const user = component.props().user;
    expect(user.name).toEqual('teste');
  });
});
