import { connect } from 'react-redux';

const mapState = state => ({
  user: state.user,
});

const UserContainer = WrappedComponent => connect(mapState)(WrappedComponent);

export default UserContainer;
