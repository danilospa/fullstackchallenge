export const CREDIT_CARD_NUMBER = [
  { char: /\d/, repeat: 4 },
  { exactly: ' ' },
  { char: /\d/, repeat: 4 },
  { exactly: ' ' },
  { char: /\d/, repeat: 4 },
  { exactly: ' ' },
  { char: /\d/, repeat: 4 },
];

export const CREDIT_CART_EXPIRATION = [
  { char: /\d/, repeat: 2 },
  { exactly: '/' },
  { char: /\d/, repeat: 2 },
];

export const CREDIT_CARD_CVV = [{ char: /\d/, repeat: 4 }];

export const CPF = [
  { char: /\d/, repeat: 3 },
  { exactly: '.' },
  { char: /\d/, repeat: 3 },
  { exactly: '.' },
  { char: /\d/, repeat: 3 },
  { exactly: '-' },
  { char: /\d/, repeat: 2 },
];

export const PHONE = [
  { exactly: '(' },
  { char: /\d/, repeat: 2 },
  { exactly: ')' },
  { exactly: ' ' },
  { char: /\d/, repeat: 5 },
  { exactly: '-' },
  { char: /\d/, repeat: 4 },
];

export const ZIPCODE = [
  { char: /\d/, repeat: 5 },
  { exactly: '-' },
  { char: /\d/, repeat: 3 },
];
