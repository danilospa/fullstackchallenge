import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom'
import AppHeaderContainer from './containers/AppHeader/index';
import SessionContainer from './containers/Session/index';
import ProductsListContainer from './containers/ProductsList/index';
import CreateProduct from './components/CreateProduct/index';
import Cart from './components/Cart/index';
import CheckoutSuccess from './components/CheckoutSuccess/index';

const Routes = () => (
  <div>
    <SessionContainer />
    <AppHeaderContainer />
    <Switch>
      <Route exact path="/" component={ProductsListContainer} />
      <Route exact path="/products/create" component={CreateProduct} />
      <Route exact path="/cart" component={Cart} />
      <Route exact path="/checkout/success" component={CheckoutSuccess} />
      <Route render={() => (<Redirect to="/" />)} />
    </Switch>
  </div>
);

export default Routes;
