import subject from './index';
import axios from 'axios';
import { BIGFIN_API_BASE_URL } from '../../config'

jest.mock('axios');

describe('bigfinApi client', () => {
  describe('facebook login', () => {
    it('performs correct request', () => {
      subject.facebookLogin('token');
      expect(axios.post).toBeCalledWith(`${BIGFIN_API_BASE_URL}/session/facebook`, { token: 'token' });
    });

    it('returns correct value', () => {
      axios.post.mockReturnValue('facebook login response');
      expect(subject.facebookLogin('token')).toEqual('facebook login response');
    });
  });

  describe('fetch current session', () => {
    it('performs correct request', () => {
      subject.fetchCurrentSession('token');
      expect(axios.get).toBeCalledWith(`${BIGFIN_API_BASE_URL}/session/info`, {
        headers: {
          authorization: 'token',
        }
      });
    });

    it('returns correct value', () => {
      axios.get.mockReturnValue('fetch session response');
      expect(subject.fetchCurrentSession('token')).toEqual('fetch session response');
    });
  });

  describe('fetch products', () => {
    it('performs correct request', () => {
      subject.fetchProducts('token');
      expect(axios.get).toBeCalledWith(`${BIGFIN_API_BASE_URL}/products`, {
        headers: {
          authorization: 'token',
        }
      });
    });

    it('returns correct value', () => {
      axios.get.mockReturnValue('fetch product response');
      expect(subject.fetchProducts('token')).toEqual('fetch product response');
    });
  });

  describe('create product', () => {
    it('performs correct request', () => {
      subject.createProduct('token', 'product');
      expect(axios.post).toBeCalledWith(`${BIGFIN_API_BASE_URL}/products`, 'product', {
        headers: {
          authorization: 'token',
        }
      });
    });

    it('returns correct value', () => {
      axios.post.mockReturnValue('create productresponse');
      expect(subject.createProduct('token', 'product')).toEqual('create productresponse');
    });
  });

  describe('calculates cart', () => {
    it('performs correct request', () => {
      subject.calculateCart('token', 'items');
      expect(axios.post).toBeCalledWith(`${BIGFIN_API_BASE_URL}/cart`, { items: 'items' }, {
        headers: {
          authorization: 'token',
        }
      });
    });

    it('returns correct value', () => {
      axios.post.mockReturnValue('calculate cart response');
      expect(subject.calculateCart('token', 'product')).toEqual('calculate cart response');
    });
  });

  describe('get cart', () => {
    it('performs correct request', () => {
      subject.getCart('token');
      expect(axios.get).toBeCalledWith(`${BIGFIN_API_BASE_URL}/cart`, {
        headers: {
          authorization: 'token',
        }
      });
    });

    it('returns correct value', () => {
      axios.get.mockReturnValue('get cart response');
      expect(subject.getCart('token')).toEqual('get cart response');
    });
  });

  describe('create order', () => {
    it('performs correct request', () => {
      subject.createOrder('token', 'order data');
      expect(axios.post).toBeCalledWith(`${BIGFIN_API_BASE_URL}/order`, 'order data', {
        headers: {
          authorization: 'token',
        }
      });
    });

    it('returns correct value', () => {
      axios.get.mockReturnValue('create order response');
      expect(subject.getCart('token', 'order data')).toEqual('create order response');
    });
  });
});
