import axios from 'axios';
import { BIGFIN_API_BASE_URL } from '../../config'

function facebookLogin(token) {
  return axios.post(`${BIGFIN_API_BASE_URL}/session/facebook`, { token });
}

function fetchCurrentSession(token) {
  return axios.get(`${BIGFIN_API_BASE_URL}/session/info`, {
    headers: {
      authorization: token,
    },
  });
}

function fetchProducts(token) {
  return axios.get(`${BIGFIN_API_BASE_URL}/products`, {
    headers: {
      authorization: token,
    },
  });
}

function createProduct(token, product) {
  return axios.post(`${BIGFIN_API_BASE_URL}/products`, product, {
    headers: {
      authorization: token,
    },
  });
}

function calculateCart(token, items) {
  return axios.post(`${BIGFIN_API_BASE_URL}/cart`, { items }, {
    headers: {
      authorization: token,
    },
  });
}

function getCart(token) {
  return axios.get(`${BIGFIN_API_BASE_URL}/cart`, {
    headers: {
      authorization: token,
    },
  });
}

function uploadImage(token, data) {
  return axios.post(`${BIGFIN_API_BASE_URL}/images`, data, {
      headers: {
        'content-type': 'multipart/form-data',
        authorization: token
      }
    }
  );
}

function createOrder(token, data) {
  return axios.post(`${BIGFIN_API_BASE_URL}/order`, data, {
    headers: {
      authorization: token,
    },
  });
}

export default {
  facebookLogin, fetchCurrentSession, fetchProducts, createProduct, calculateCart, getCart, uploadImage, createOrder
};
