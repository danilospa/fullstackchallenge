# Bigfin-Web

This is the frontend application of Bigfin e-commerce.  
[Rematch](https://github.com/rematch/rematch) was used on this project in top of Redux. Rematch is a library that abstract Redux reducers and actions in a more simple way. You can read more about it in [this](https://hackernoon.com/redesigning-redux-b2baee8b8a38) post.

## Dependencies

- NodeJS 8.9.3
- Bigfin-api

## How to develop

First of all, make sure you have the correct version of Node, then install [Yarn](https://yarnpkg.com/en/docs/install#mac-stable). After, install all the dependencies:
```bash
$ yarn install
```

Copy the configuration file and change it as needed:
```bash
$ cp .env.local.example .env.local
```

To start up the server:
```bash
$ yarn start
```

The server should be up and listening on port 3000.

To run the tests, execute:
```bash
$ yarn test
```

## How to deploy

Production deploy is made automatically by [CircleCI](https://circleci.com/bb/danilospa/fullstackchallenge) when merging into master branch.  
The static files are send to a AWS S3 bucket. They are served through AWS CloudFront.
