const nock = require('nock');
const config = require('../../config/app');
const subject = require('../../src/clients/pagar-me');

const env = process.env.NODE_ENV || 'development';

describe('PagarMe client', () => {
  describe('transaction endpoint', () => {
    const data = { field: 'value' };

    beforeEach(() => {
      nock('https://api.pagar.me/1')
        .post(`/transactions?api_key=${config[env].pagarMeApiKey}`, data)
        .reply(200, 'response');
    });

    it('returns correct value', async () => {
      const response = await subject.createTransaction(data);
      expect(response.status).toEqual(200);
      expect(response.data).toEqual('response');
    });
  });
});
