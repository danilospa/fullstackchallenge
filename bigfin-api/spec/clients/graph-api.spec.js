const nock = require('nock');
const subject = require('../../src/clients/graph-api');

describe('GraphApi client', () => {
  describe('me endpoint', () => {
    const token = 'token';
    const fields = ['field1', 'field2'];

    beforeEach(() => {
      nock('https://graph.facebook.com')
        .get(`/v2.12/me?fields=field1,field2&access_token=${token}`)
        .reply(200, 'response');
    });

    it('returns correct value', async () => {
      const response = await subject.me(token, fields);
      expect(response.status).toEqual(200);
      expect(response.data).toEqual('response');
    });
  });
});
