const subject = require('../../src/resources/user');
const { User } = require('../../models');

describe('User resource', () => {
  it('returns only allowed fields when given user is a model', () => {
    const user = User.build({
      name: 'john',
      email: 'john@gmail.com',
      role: 'role for john',
    });
    expect(subject(user)).toEqual({
      name: 'john',
      email: 'john@gmail.com',
    });
  });

  it('returns only allowed fields when given user a json', () => {
    const user = {
      name: 'john',
      email: 'john@gmail.com',
      role: 'role for john',
    };
    expect(subject(user)).toEqual({
      name: 'john',
      email: 'john@gmail.com',
    });
  });

  it('returns role when user is admin', () => {
    const user = {
      role: 'admin',
    };
    expect(subject(user)).toEqual({
      role: 'admin',
    });
  });
});
