const subject = require('../../src/middlewares/authenticate-user');
const redisClient = require('../../src/infrastructure/redis');

function mockAuthorizationHeader(req, value) {
  req.get.andCallFake(header => header === 'authorization' && value);
}

describe('Authenticate middleware', () => {
  let req;
  let res;
  const next = jasmine.createSpy();

  beforeEach(() => {
    req = {
      get: jasmine.createSpy(),
    };
    res = {
      status: jasmine.createSpy(),
      end: jasmine.createSpy(),
    };
    res.status.andCallFake(() => res);
  });

  it('returns 403 when authorization header is not provided', async (done) => {
    mockAuthorizationHeader(req, null);
    await subject(req, res, next);
    expect(res.status).toHaveBeenCalledWith(403);
    expect(res.end).toHaveBeenCalledWith();
    done();
  });

  describe('when authorization header is provided', () => {
    beforeEach(() => mockAuthorizationHeader(req, 'hash'));

    it('returns 403 when hash does not exist on cache', async (done) => {
      spyOn(redisClient, 'getAsync').andReturn(new Promise(resolve => resolve()));
      await subject(req, res, next);
      expect(res.status).toHaveBeenCalledWith(403);
      expect(res.end).toHaveBeenCalledWith();
      done();
    });

    describe('when hash does exist on cache', () => {
      let cacheData;
      beforeEach(() => {
        cacheData = JSON.stringify({
          data: 'value',
        });
        spyOn(redisClient, 'getAsync').andReturn(new Promise(resolve => resolve(cacheData)));
      });

      it('allows request to continue', async (done) => {
        await subject(req, res, next);
        expect(next).toHaveBeenCalledWith();
        done();
      });

      it('stores cache data into the request', async (done) => {
        await subject(req, res, next);
        expect(req.session).toEqual(JSON.parse(cacheData));
        done();
      });
    });
  });
});
