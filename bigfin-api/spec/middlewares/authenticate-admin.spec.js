const subject = require('../../src/middlewares/authenticate-admin');

describe('Authenticate middleware', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    next = jasmine.createSpy();
    req = {};
    res = {
      status: jasmine.createSpy(),
      end: jasmine.createSpy(),
    };
    res.status.andCallFake(() => res);
  });

  it('allows request to continue if user logged is admin', () => {
    req.session = { role: 'admin' };
    subject(req, res, next);
    expect(next).toHaveBeenCalledWith();
  });

  it('returns 403 when user logged is not admin', async (done) => {
    req.session = { role: 'customer' };
    await subject(req, res, next);
    expect(res.status).toHaveBeenCalledWith(403);
    expect(res.end).toHaveBeenCalledWith();
    done();
  });
});
