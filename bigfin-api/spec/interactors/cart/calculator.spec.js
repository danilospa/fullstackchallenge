const subject = require('../../../src/interactors/cart/calculator');
const factorDiscounts = require('../../../src/config/factor-discounts');

describe('Calculator cart interactor', () => {
  const cart = {
    items: [],
  };

  beforeEach(() => {
    const mockedFactors = {
      X: {
        discount: 0.01,
        max: 0.05,
      },
      Z: {
        discount: 0.5,
        max: 0.5,
      },
    };
    spyOn(factorDiscounts, 'get').andReturn(mockedFactors);

    cart.items = [
      {
        product: {
          factor: 'X',
          price: 200,
        },
        quantity: 2,
      },
      {
        product: {
          factor: 'X',
          price: 200,
        },
        quantity: 3,
      },
    ];
  });

  it('should give correct progressive discount for when all products are from the same factor', () => {
    const cartValues = subject.call(cart);
    expect(cartValues.discountsByFactor).toEqual(50);
  });

  it('should limit discount with correct value', () => {
    cart.items.push({
      product: {
        factor: 'X',
        price: 100,
      },
      quantity: 1,
    });
    const cartValues = subject.call(cart);
    expect(cartValues.discountsByFactor).toEqual(55);
  });

  it('should return correct sub total value', () => {
    const cartValues = subject.call(cart);
    expect(cartValues.subTotal).toEqual(1000);
  });

  it('should return correct total value', () => {
    const cartValues = subject.call(cart);
    expect(cartValues.total).toEqual(950);
  });

  it('should give correct progressive discount when cart has products of different factors', () => {
    cart.items.push({
      product: {
        factor: 'Z',
        price: 100,
      },
      quantity: 2,
    });
    const cartValues = subject.call(cart);
    expect(cartValues.discountsByFactor).toEqual(150);
  });
});
