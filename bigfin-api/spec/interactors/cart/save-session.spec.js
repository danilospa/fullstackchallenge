const subject = require('../../../src/interactors/cart/save-session');
const redisClient = require('../../../src/infrastructure/redis');

describe('SaveSession cart interactor', () => {
  it('updates session on cache with new cart', () => {
    spyOn(redisClient, 'setAsync');
    const session = {
      token: 'token',
    };
    subject.call(session, 'cart info');
    expect(redisClient.setAsync).toHaveBeenCalledWith(session.token, JSON.stringify({
      token: session.token,
      cart: 'cart info',
    }));
  });
});
