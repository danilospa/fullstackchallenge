const subject = require('../../../src/interactors/session/facebook-login');
const graphApi = require('../../../src/clients/graph-api');
const redisClient = require('../../../src/infrastructure/redis');
const { User } = require('../../../models');

describe('FacebookLogin session interactor', () => {
  const token = 'token';
  let user;

  beforeEach(() => {
    spyOn(redisClient, 'setAsync').andReturn(new Promise(resolve => resolve()));
    const graphApiResponse = {
      data: {
        id: 1,
        name: 'john',
      },
    };
    user = User.build({
      name: 'john',
    });
    spyOn(graphApi, 'me').andReturn(new Promise(resolve => resolve(graphApiResponse)));
    spyOn(User, 'findByFacebookId').andReturn(new Promise(resolve => resolve(user)));
    spyOn(User, 'create').andReturn(new Promise(resolve => resolve(user)));
  });

  it('sets correct data on redis', async (done) => {
    await subject.call(token);
    const expectedRedisData = JSON.stringify(Object.assign({}, user.dataValues, { token }));
    expect(redisClient.setAsync).toHaveBeenCalledWith(token, expectedRedisData);
    done();
  });

  it('returns correct data', async (done) => {
    const response = await subject.call(token);
    expect(response.name).toEqual('john');
    done();
  });
});
