const subject = require('../../../src/interactors/order/create');
const pagarMeClient = require('../../../src/clients/pagar-me');

describe('Create order interactor', () => {
  it('calls pagarme with correct data', () => {
    spyOn(pagarMeClient, 'createTransaction');

    const data = {
      customerInfo: {
        documentNumber: 'document number',
        phoneNumber: 'phone number',
      },
      shippingAddress: {},
      creditCardInfo: {
        cardHolderName: 'card owner',
      },
    };
    const cart = {
      total: 100,
      items: [{
        quantity: 'quantity',
        product: {
          id: 'product id',
          name: 'product name',
          price: 10,
        },
      }],
    };
    const session = {
      id: 1,
      name: 'john',
      email: 'john@gmail.com',
    };

    subject.call(data, cart, session);
    expect(pagarMeClient.createTransaction).toHaveBeenCalledWith({
      card_holder_name: 'card owner',
      amount: 10000,
      items: [{
        id: 'product id',
        title: 'product name',
        unit_price: 1000,
        quantity: 'quantity',
        tangible: false,
      }],
      billing: {
        address: {
          country: 'br',
        },
        name: 'john',
      },
      customer: {
        country: 'br',
        documents: [
          {
            number: 'document number',
            type: 'cpf',
          },
        ],
        email: 'john@gmail.com',
        external_id: '1',
        name: 'john',
        phone_numbers: ['phone number'],
        type: 'individual',
      },
    });
  });
});
