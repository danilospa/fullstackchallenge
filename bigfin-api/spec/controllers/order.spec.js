const nock = require('nock');
const application = require('../../');
const request = require('supertest')(application);
const models = require('../../models');
const config = require('../../config/app');
const redisClient = require('../../src/infrastructure/redis');

const env = process.env.NODE_ENV || 'development';

require('jasmine-before-all');

function createOrder(token, payload) {
  return request.post('/order').set('authorization', token).send(payload);
}

async function createSampleProduct() {
  const product = await models.Product.create({
    name: 'product test',
    price: 100,
    factor: 'A',
  });

  nock('https://api.pagar.me/1')
    .post(`/transactions?api_key=${config[env].pagarMeApiKey}`, {
      amount: 19600,
      items: [{
        id: `${product.id}`,
        title: product.name,
        unit_price: 10000,
        quantity: 2,
        tangible: false,
      }],
      billing: {
        address: { country: 'br' },
      },
      customer: {
        country: 'br',
        documents: [{
          type: 'cpf',
        }],
        external_id: 'undefined',
        phone_numbers: [null],
        type: 'individual',
      },
    })
    .reply(200);

  return product;
}

describe('OrderController', () => {
  const token = 'token';

  beforeEach(() => {
    models.Product.destroyAll();
  });

  describe('POST /order', () => {
    it('returns 403 with empty body when token is not on session', async (done) => {
      await redisClient.delAsync(token);
      createOrder(token).expect(403, {}, done);
    });

    describe('when user is logged in', () => {
      const payload = {
        customerInfo: {},
        shippingAddress: {},
        creditCardInfo: {},
      };

      beforeEach(() => {
        payload.items = [{
          quantity: 2,
        }];
      });

      it('returns 200', async (done) => {
        const product = await createSampleProduct();

        payload.items[0].id = product.id;
        await redisClient.setAsync(token, JSON.stringify({ token }));
        createOrder(token, payload).expect(200, done);
      });
    });
  });

  afterAll(() => {
    application.close();
  });
});

