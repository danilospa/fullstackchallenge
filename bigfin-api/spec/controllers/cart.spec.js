const application = require('../../');
const request = require('supertest')(application);
const models = require('../../models');
const redisClient = require('../../src/infrastructure/redis');

require('jasmine-before-all');

function listCart(token) {
  return request.get('/cart').set('authorization', token);
}

function createCart(token, payload) {
  return request.post('/cart').set('authorization', token).send(payload);
}

function createSampleProduct() {
  return models.Product.create({
    name: 'product test',
    price: 100,
    factor: 'A',
  });
}

describe('CartController', () => {
  const token = 'token';

  beforeEach(() => {
    models.Product.destroyAll();
  });

  describe('GET /cart', () => {
    it('returns 403 with empty body when token is not on session', async (done) => {
      await redisClient.delAsync(token);
      listCart(token).expect(403, {}, done);
    });

    describe('when user is logged in', () => {
      let session;

      beforeEach(() => {
        session = {
          cart: [{
            id: null,
            quantity: 2,
          }],
        };
      });

      it('returns 200', async (done) => {
        const product = await createSampleProduct();
        session.cart[0].id = product.id;
        await redisClient.setAsync(token, JSON.stringify(session));
        listCart(token).expect(200, done);
      });

      it('returns current cart calculated', async (done) => {
        const product = await createSampleProduct();
        session.cart[0].id = product.id;
        await redisClient.setAsync(token, JSON.stringify(session));
        listCart(token).then((response) => {
          expect(response.body.items[0].product.name).toEqual('product test');
          expect(response.body.items[0].quantity).toEqual(2);
          expect(response.body.subTotal).toEqual(200);
          expect(response.body.total).toEqual(196);
          done();
        });
      });
    });
  });

  describe('POST /cart', () => {
    it('returns 403 with empty body when token is not on session', async (done) => {
      await redisClient.delAsync(token);
      createCart(token).expect(403, {}, done);
    });

    describe('when user is logged in', () => {
      const payload = {};

      beforeEach(() => {
        payload.items = [{
          quantity: 2,
        }];
      });

      it('returns 200', async (done) => {
        await createSampleProduct();
        await redisClient.setAsync(token, JSON.stringify({ token }));
        createCart(token, payload).expect(200, done);
      });

      it('returns current cart calculated', async (done) => {
        const product = await createSampleProduct();
        payload.items[0].id = product.id;
        await redisClient.setAsync(token, JSON.stringify({ token }));
        createCart(token, payload).then((response) => {
          expect(response.body.items[0].product.name).toEqual('product test');
          expect(response.body.items[0].quantity).toEqual(2);
          expect(response.body.subTotal).toEqual(200);
          expect(response.body.total).toEqual(196);
          done();
        });
      });

      it('sets new cart on session', async (done) => {
        const product = await createSampleProduct();
        payload.items[0].id = product.id;
        await redisClient.setAsync(token, JSON.stringify({ token }));
        createCart(token, payload).then(async () => {
          const session = JSON.parse(await redisClient.getAsync(token));
          expect(session.cart).toEqual(payload.items);
          done();
        });
      });
    });
  });

  afterAll(() => {
    application.close();
  });
});

