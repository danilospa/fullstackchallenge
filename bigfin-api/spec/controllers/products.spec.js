const application = require('../../');
const request = require('supertest')(application);
const models = require('../../models');
const redisClient = require('../../src/infrastructure/redis');

require('jasmine-before-all');

function listProducts() {
  return request.get('/products');
}

function createProduct(token) {
  return request.post('/products').set('authorization', token).send({ name: 'product name' });
}

describe('SessionController', () => {
  beforeEach(() => {
    models.Product.destroyAll();
  });

  describe('GET /products', () => {
    it('returns 200', (done) => {
      listProducts().expect(200, done);
    });

    it('returns list of products', async (done) => {
      const firstProduct = await models.Product.create();
      const secondProduct = await models.Product.create();
      listProducts().then((response) => {
        expect(response.body.products.map(p => p.id)).toEqual([firstProduct.id, secondProduct.id]);
        done();
      });
    });
  });

  describe('POST /products', () => {
    const token = 'token';

    describe('when user is not admin', () => {
      it('returns 403', async (done) => {
        await redisClient.set(token, JSON.stringify({ role: 'customer' }));
        createProduct(token).expect(403, done);
      });
    });

    describe('when user is admin', () => {
      it('returns 201', async (done) => {
        await redisClient.set(token, JSON.stringify({ role: 'admin' }));
        createProduct(token).expect(201, done);
      });

      it('return created product', async (done) => {
        createProduct(token).then((response) => {
          expect(response.body.product.name).toEqual('product name');
          done();
        });
      });
    });
  });

  afterAll(() => {
    application.close();
  });
});

