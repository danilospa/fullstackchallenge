const application = require('../../');
const request = require('supertest')(application);

require('jasmine-before-all');

describe('GET /', () => {
  it('returns status code 200', (done) => {
    request.get('/').expect(200, done);
  });

  it('returns correct body', (done) => {
    request.get('/').then((response) => {
      expect(response.body).toEqual({ status: 'OK' });
      done();
    });
  });

  afterAll(() => {
    application.close();
  });
});
