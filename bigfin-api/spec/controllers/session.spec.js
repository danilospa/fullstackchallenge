const nock = require('nock');
const application = require('../../');
const request = require('supertest')(application);
const models = require('../../models');
const redisClient = require('../../src/infrastructure/redis');

require('jasmine-before-all');

function facebookLogin(token) {
  return request.post('/session/facebook').send({ token });
}

function sessionInfo(token) {
  return request.get('/session/info').set('authorization', token);
}

describe('SessionController', () => {
  describe('POST /session/facebook', () => {
    let token;

    beforeEach(() => {
      models.User.destroyAll();
    });

    describe('when token is valid', () => {
      beforeEach(() => {
        token = 'graph-api-token';

        nock('https://graph.facebook.com')
          .get(`/v2.12/me?fields=name,email&access_token=${token}`)
          .reply(200, { id: 1, name: 'john', email: 'john@gmail.com' });
      });

      describe('when user is not registred', () => {
        it('returns status code 200', (done) => {
          facebookLogin(token).expect(200, done);
        });

        it('returns correct body', (done) => {
          facebookLogin(token).then((response) => {
            expect(response.body.user).toMatch({
              name: 'john',
              email: 'john@gmail.com',
            });
            done();
          });
        });

        it('creates user on database', (done) => {
          facebookLogin(token).then(async () => {
            const user = await models.User.findByFacebookId(1);
            expect(user.name).toEqual('john');
            expect(user.email).toEqual('john@gmail.com');
            expect(user.facebookId).toEqual('1');
            done();
          });
        });

        it('stores user info on redis', (done) => {
          facebookLogin(token).then(() => {
            redisClient.get(token, (err, value) => {
              const user = JSON.parse(value);
              expect(user.name).toEqual('john');
              expect(user.email).toEqual('john@gmail.com');
              expect(user.facebookId).toEqual('1');
              done();
            });
          });
        });
      });

      describe('when user is registred', () => {
        beforeEach(() => {
          models.User.create({
            name: 'john',
            email: 'john@gmail.com',
            facebookId: 1,
          });
        });

        it('returns status code 200', (done) => {
          facebookLogin(token).expect(200, done);
        });

        it('returns correct body', (done) => {
          facebookLogin(token).then((response) => {
            expect(response.body.user).toMatch({
              name: 'john',
              email: 'john@gmail.com',
            });
            done();
          });
        });
      });
    });

    describe('when token is not valid', () => {
      beforeEach(() => {
        token = 'graph-api-token';

        nock('https://graph.facebook.com')
          .get(`/v2.12/me?fields=name,email&access_token=${token}`)
          .reply(400);
      });

      it('returns status code 401 with empty json', (done) => {
        facebookLogin(token).expect(401, {}, done);
      });
    });

    describe('when occurs some error on graph api', () => {
      beforeEach(() => {
        token = 'graph-api-token';

        nock('https://graph.facebook.com')
          .get(`/v2.12/me?fields=name,email&access_token=${token}`)
          .reply(500);
      });

      it('returns status code 424 with empty json', (done) => {
        facebookLogin(token).expect(424, {}, done);
      });
    });
  });

  describe('GET /session/info', () => {
    const token = 'token';

    it('returns 403 with empty body when token is not on session', (done) => {
      redisClient.del(token);
      sessionInfo(token).expect(403, {}, done);
    });

    it('returns 200 with correct user info when token is on session', (done) => {
      redisClient.set(token, JSON.stringify({ name: 'user name' }));
      sessionInfo(token).expect(200, { name: 'user name' }, done);
    });
  });

  afterAll(() => {
    application.close();
    redisClient.quit();
    models.sequelize.close();
  });
});

