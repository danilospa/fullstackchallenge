const _ = require('lodash');
const subject = require('../../src/config/factor-discounts');

describe('FactorDiscounts config', () => {
  it('verifies if each factor has a discount value', () => {
    _.map(subject.get(), (v) => {
      const hasDiscountProperty = Object.prototype.hasOwnProperty.call(v, 'discount');
      expect(hasDiscountProperty).toBe(true);
    });
  });

  it('verifies if each factor has a max discount value', () => {
    _.map(subject.get(), (v) => {
      const hasMaxProperty = Object.prototype.hasOwnProperty.call(v, 'max');
      expect(hasMaxProperty).toBe(true);
    });
  });
});
