# Bigfin-api

API responsible for all the business logic of Bigfin e-commerce.

## Dependencies

- NodeJS 8.9.3
- Yarn
- PostgreSQL
- Redis

## How to develop

First of all, make sure you have the correct version of Node, then install [Yarn](https://yarnpkg.com/en/docs/install#mac-stable). After, install all the dependencies:
```bash
$ yarn install
```

Make sure you have PostgreSQL and Redis installed. Then, copy the configuration files and edit them as needed:
```bash
$ cp config/database.js.example config/database.js
$ cp config/redis.js.example config/redis.js
$ cp config/app.js.example config/app.js
```

To create the database, run:
```bash
$ yarn run db:create
$ NODE_ENV=test yarn run db:create
```

Then, run the migrations:
```bash
$ yarn run db:migrate
$ NODE_ENV=test yarn run db:migrate
```

To start up the server:
```bash
$ yarn start
```

Or you can start it with auto reload for file changes:
```bash
$ yarn run dev
```

The server should be up and listening on port 3002.

To run the tests, execute:
```bash
$ yarn test
```

Or if you want to watch for files changes:
```bash
$ yarn run dev-test
```

To run the linter:
```bash
$ yarn run lint
```

*Observation about tests: controllers tests act as a integration test instead of unit test.

## Running with docker

You can run bigfin-api and its dependencies using docker-compose. The first user to perform the login will become the administrator.
In order to upload images and to perform checkout, you need to run the command export the following environment variables:
  - PAGAR_ME_API_KEY
  - CLOUDNARY_CLOUD_NAME 
  - CLOUDNARY_API_KEY 
  - CLOUDNARY_API_SECRET 

```bash
$ docker-compose up --build
```

## How to deploy

Production deploy is made automatically by [CircleCI](https://circleci.com/bb/danilospa/fullstackchallenge) when merging into master branch.  
After the continuous integration builds and pushes the docker image to [Dockerhub](https://hub.docker.com/r/danilospa/bigfin-api/), [Watchtower](https://github.com/v2tec/watchtower) verifies for updates on the image, pull it and start again.  
Watchtower and bigfin-api currently runs on the same AWS EC2.
