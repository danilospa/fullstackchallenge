'use strict';
module.exports = (sequelize, DataTypes) => {
  const { Op } = sequelize;
  var User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    facebookId: DataTypes.BIGINT,
    role: DataTypes.ENUM('admin', 'customer')
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  User.findByFacebookId = function(id) {
    return this.find({
      where: {
        facebookId: {
          [Op.eq]: id,
        },
      }
    });
  };
  User.destroyAll = function() {
    return this.destroy({
      where: {},
      truncate: true,
    });
  };
  return User;
};
