'use strict';
module.exports = (sequelize, DataTypes) => {
  const { Op } = sequelize;
  var Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    image: DataTypes.STRING,
    price: DataTypes.FLOAT,
    factor: DataTypes.ENUM('A', 'B', 'C')
  }, {});
  Product.associate = function(models) {
    // associations can be defined here
  };
  Product.destroyAll = function() {
    return this.destroy({
      where: {},
      truncate: true,
    });
  };
  Product.findAllByIds = function(ids) {
    return this.findAll({
      where: {
        id: {
          [Op.in]: ids,
        }
      }
    });
  };
  return Product;
};
