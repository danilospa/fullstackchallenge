const express = require('express');

const routes = new express.Router();
const healthChecksController = require('./controllers/health-checks');
const sessionController = require('./controllers/session');
const productsController = require('./controllers/products');
const cartController = require('./controllers/cart');
const orderController = require('./controllers/order');
const imagesController = require('./controllers/images');
const authenticateUserMiddleware = require('./middlewares/authenticate-user');
const authenticateAdminMiddleware = require('./middlewares/authenticate-admin');

routes.get('/', healthChecksController.index);
routes.post('/session/facebook', sessionController.facebook);
routes.get('/products', productsController.index);

routes.use(authenticateUserMiddleware);
routes.get('/session/info', sessionController.info);
routes.get('/cart', cartController.index);
routes.post('/cart', cartController.create);
routes.post('/order', orderController.create);

routes.use(authenticateAdminMiddleware);
routes.post('/products', productsController.create);
routes.post('/images', imagesController.create);

module.exports = routes;
