const redis = require('redis');
const bluebird = require('bluebird');

const env = process.env.NODE_ENV || 'development';
const config = require('../../config/redis')[env];

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisClient = redis.createClient({
  host: config.host,
  post: config.port,
});

module.exports = redisClient;
