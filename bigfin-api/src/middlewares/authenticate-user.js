const redisClient = require('../infrastructure/redis');

function respondsForbidden(res) {
  res.status(403).end();
}

async function authenticate(req, res, next) {
  const authentication = req.get('authorization');
  if (!authentication) {
    return respondsForbidden(res);
  }

  const value = await redisClient.getAsync(authentication);
  if (value) {
    req.session = JSON.parse(value);
    next();
  } else {
    return respondsForbidden(res);
  }
}

module.exports = authenticate;
