function respondsForbidden(res) {
  res.status(403).end();
}

function authenticate(req, res, next) {
  return req.session.role === 'admin' ? next() : respondsForbidden(res);
}

module.exports = authenticate;
