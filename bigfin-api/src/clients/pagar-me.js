const axios = require('axios');
const config = require('../../config/app');

const env = process.env.NODE_ENV || 'development';

const PAGAR_ME_BASE_URL = 'https://api.pagar.me/1';
const PAGAR_ME_API_KEY = config[env].pagarMeApiKey;

function createTransaction(data) {
  return axios.post(`${PAGAR_ME_BASE_URL}/transactions?api_key=${PAGAR_ME_API_KEY}`, data);
}

module.exports = { createTransaction };
