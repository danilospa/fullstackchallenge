const axios = require('axios');

const GRAPH_API_BASE_URL = 'https://graph.facebook.com/v2.12';

function me(token, fields = []) {
  return axios.get(`${GRAPH_API_BASE_URL}/me?fields=${fields.join(',')}&access_token=${token}`);
}

module.exports = { me };
