const cloudinary = require('cloudinary');

const env = process.env.NODE_ENV || 'development';
const config = require('../../config/app')[env].cloudinary;

cloudinary.config(config);

function upload(path) {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader.upload(path, {}, (err, result) => (err ? reject(err) : resolve(result)));
  });
}

module.exports = { upload };
