const factorsDiscounts = {
  A: {
    discount: 0.01,
    max: 0.05,
  },
  B: {
    discount: 0.05,
    max: 0.15,
  },
  C: {
    discount: 0.1,
    max: 0.3,
  },
};

function get() {
  return factorsDiscounts;
}

module.exports = { get };
