const _ = require('lodash');

function snakelize(string) {
  return string.replace(/[A-Z]/g, m => `_${m.toLowerCase()}`);
}

function snakelizeItem(item) {
  return item.constructor === Array ? snakelizeArray(item) : snakelizeKeys(item); // eslint-disable-line no-use-before-define, max-len
}

function snakelizeArray(array) {
  return array.map(item => (typeof item === 'object'
    ? snakelizeItem(item)
    : item));
}

function snakelizeKeys(obj) {
  const objSnackelized = {};
  _.forEach((obj), (v, k) => {
    objSnackelized[snakelize(k)] = typeof v === 'object'
      ? snakelizeItem(v)
      : v;
  });

  return objSnackelized;
}

module.exports = { snakelizeKeys };
