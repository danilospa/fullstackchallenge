const _ = require('lodash');
const factorsDiscounts = require('../../config/factor-discounts');

function call(cart) {
  const { items } = cart;

  const subTotal = items.reduce((sum, i) => sum + (i.quantity * i.product.price), 0);

  const discounts = _.map(factorsDiscounts.get(), (factorInfo, factorType) => {
    const factorItems = items.filter(i => i.product.factor === factorType);
    const totalProducts = factorItems.reduce((sum, i) => sum + i.quantity, 0);

    const priceWithoutDiscount = factorItems.reduce((sum, i) => sum + (i.quantity * i.product.price), 0); // eslint-disable-line max-len
    const factorDiscount = factorInfo.discount * totalProducts;
    const maxDiscount = factorInfo.max;
    const discount = priceWithoutDiscount * (Math.min(factorDiscount, maxDiscount));

    return discount;
  });

  const discountsByFactor = discounts.reduce((sum, d) => sum + d, 0);

  return {
    subTotal,
    discountsByFactor,
    total: subTotal - discountsByFactor,
  };
}

module.exports = { call };
