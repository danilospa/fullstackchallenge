const redisClient = require('../../infrastructure/redis');

function call(session, items) {
  const newSession = Object.assign({}, session, { cart: items });
  return redisClient.setAsync(session.token, JSON.stringify(newSession));
}

module.exports = { call };
