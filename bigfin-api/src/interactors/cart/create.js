const { Product } = require('../../../models');
const calculatorInteractor = require('./calculator');

async function call(items) {
  const products = await Product.findAllByIds(items.map(i => i.id));
  const itemsWithProducts = products.map((p, i) => ({
    product: p,
    quantity: items[i].quantity,
  }));

  return {
    items: itemsWithProducts,
    ...calculatorInteractor.call({ items: itemsWithProducts }),
  };
}

module.exports = { call };
