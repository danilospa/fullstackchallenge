const graphApi = require('../../clients/graph-api');
const { User } = require('../../../models');
const redisClient = require('../../infrastructure/redis');

const createUser = async (data) => {
  const isFirstUser = !(await User.count());
  const role = isFirstUser ? 'admin' : 'customer';

  return User.create({
    name: data.name,
    email: data.email,
    facebookId: data.id,
    role,
  });
};

async function call(token) {
  const fields = ['name', 'email'];

  const { data } = await graphApi.me(token, fields);
  const user = await User.findByFacebookId(data.id);

  const userData = user instanceof User ? user : await createUser(data);
  const sessionData = JSON.stringify(Object.assign({}, userData.dataValues, { token }));
  await redisClient.setAsync(token, sessionData);

  return userData;
}

module.exports = { call };
