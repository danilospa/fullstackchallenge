const pagarMeClient = require('../../clients/pagar-me');
const { snakelizeKeys } = require('../../helpers/snakelize');

const buildPagarMeData = (data, cart, session) => ({
  ...data.creditCardInfo,
  amount: Math.round(cart.total * 100),
  items: cart.items.map(i => ({
    id: `${i.product.id}`,
    title: i.product.name,
    unitPrice: Math.round(i.product.price * 100),
    quantity: i.quantity,
    tangible: false,
  })),
  billing: {
    address: Object.assign({ country: 'br' }, data.shippingAddress),
    name: session.name,
  },
  customer: {
    country: 'br',
    documents: [
      {
        number: data.customerInfo.documentNumber,
        type: 'cpf',
      },
    ],
    email: session.email,
    externalId: `${session.id}`,
    name: session.name,
    phoneNumbers: [data.customerInfo.phoneNumber],
    type: 'individual',
  },
});

function call(data, cart, session) {
  return pagarMeClient.createTransaction(snakelizeKeys(buildPagarMeData(data, cart, session)));
}

module.exports = { call };
