const _ = require('lodash');
const { User } = require('../../models');

function resource(user) {
  const data = user instanceof User ? user.dataValues : user;
  const fields = ['name', 'email'];
  if (data.role === 'admin') {
    fields.push('role');
  }
  return _.pick(data, fields);
}

module.exports = resource;
