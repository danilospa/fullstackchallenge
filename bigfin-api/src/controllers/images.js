const multiparty = require('multiparty');
const cloudinary = require('../clients/cloudinary');

function create(req, res) {
  const form = new multiparty.Form();

  try {
    form.parse(req, async (err, fields, files) => {
      try {
        const { secure_url } = await cloudinary.upload(files.file[0].path); // eslint-disable-line camelcase, max-len
        res.status(200).send({ url: secure_url }); // eslint-disable-line camelcase
      } catch (e) {
        res.status(424).send({ status: 'UPLOAD_ERROR' });
      }
    });
  } catch (e) {
    res.status(400).send({});
  }
}

module.exports = { create };
