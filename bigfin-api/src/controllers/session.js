const facebookLogin = require('../interactors/session/facebook-login');
const userResource = require('../resources/user');

async function facebook(req, res) {
  const { token } = req.body;

  let response = {};
  let status;

  try {
    response = {
      user: userResource(await facebookLogin.call(token)),
    };
    status = 200;
  } catch (e) {
    switch (e.response.status) {
      case 400:
        status = 401;
        break;
      default:
        status = 424;
    }
  }
  res.status(status).send(response);
}

function info(req, res) {
  res.status(200).send(userResource(req.session));
}

module.exports = { facebook, info };
