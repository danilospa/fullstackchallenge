const { Product } = require('../../models');

async function index(req, res) {
  const products = await Product.findAll();
  res.status(200).send({ products });
}

async function create(req, res) {
  const { body } = req;
  const product = await Product.create(body);
  res.status(201).send({ product });
}

module.exports = { index, create };
