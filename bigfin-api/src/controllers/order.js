const createCartInteractor = require('../interactors/cart/create');
const saveSessionInteractor = require('../interactors/cart/save-session');
const createOrderInteractor = require('../interactors/order/create');

async function create(req, res) {
  const { body, session } = req;

  const cart = await createCartInteractor.call(body.items);
  try {
    await createOrderInteractor.call(body, cart, session);
  } catch (e) {
    return res.status(400).send({});
  }
  await saveSessionInteractor.call(session, []);

  res.status(200).send({});
}

module.exports = { create };
