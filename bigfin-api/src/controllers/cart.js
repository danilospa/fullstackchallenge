const createCartInteractor = require('../interactors/cart/create');
const saveSessionInteractor = require('../interactors/cart/save-session');

async function index(req, res) {
  const sessionCart = req.session.cart;
  const cart = await createCartInteractor.call(sessionCart || []);

  res.status(200).send(cart);
}

async function create(req, res) {
  const { body, session } = req;

  const cart = await createCartInteractor.call(body.items);
  await saveSessionInteractor.call(session, body.items);

  res.status(200).send(cart);
}

module.exports = { index, create };
