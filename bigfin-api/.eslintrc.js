module.exports = {
  rules: {
    'consistent-return': 'off'
  },
  globals: {
    it: true,
    describe: true,
    expect: true,
    beforeEach: true,
    afterAll: true,
    jasmine: true,
    spyOn: true,
  },
  extends: 'airbnb-base'
};
