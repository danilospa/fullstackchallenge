'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn(
      'Users',
      'role',
      Sequelize.ENUM('admin', 'customer')
    );
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('Users', 'role');
  }
};
