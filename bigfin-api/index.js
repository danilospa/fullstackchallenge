const app = require('express')();
const bodyParser = require('body-parser');
const routes = require('./src/routes');
const cors = require('cors');

app.use(cors())
app.use(bodyParser.json());
app.use('/', routes);

const port = process.env.APPLICATION_PORT || 3002;
const server = app.listen(port);
module.exports = server;
